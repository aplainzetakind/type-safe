Issue
```
$ cabal v2-haddock type-safe --haddock-hyperlink-source
```
at the project root to generate documentation. The output will indicate where
`index.html` is located. Navigate to it in your browser.

----

## Overview
The UI and the program logic communicate using `Control.Concurrent.Chan`. Two
`Chan`s are created, one is used to transmit key presses from the UI to the
backend, the oter is used by the backend to tell the UI what changes to make to
what's displayed. Relevant documentation for this mechanism can be found at
- [forkIO](https://hackage.haskell.org/package/base-4.12.0.0/docs/Control-Concurrent.html#v:forkIO)
- [Control.Concurrent.Chan](https://hackage.haskell.org/package/base-4.12.0.0/docs/Control-Concurrent-Chan.html)

The `Channels` type is defined in
`Tutor.TypeSafe.Types.Channels`, and `Tutor.TypeSafe.Types.Interface` contains
the `withInterface` function, which reads what type of UI to use from the
config, sets up the `Chan`s, then starts the UI and `Tutor.Typist.Drill.drill`,
with those `Chan`s.

### TODO
#### Issues
- `ENTER` is not handled properly. It introduces a line
break which then cannot be deleted. The UI may need to keep
a state of line lengths (or an entire buffer) to know where
to jump back.
#### Planned functionality
- A way to detect or at least specify system keymap, then to translate to any
  other keymap in the program, thus providing the ability to practice a keymap
  without setting it system-wide.
- A database to record user performance.
- More types of exercises. Specifically, something which targets user's
  weaknesses based on the database. Preferably, this should be langauge aware,
  in the sense that it should target character sequences which would result in
  most CPM improvements for the target language.
- A GUI.
