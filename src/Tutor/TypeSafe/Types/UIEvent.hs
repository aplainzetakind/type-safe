{-|
Module      : Tutor.TypeSafe.Types.UIEvent
Stability   : experimental

The command line interface.
-}
module Tutor.TypeSafe.Types.UIEvent where

-- | Holds the session and last prompt average CPM's.
data Averages = Averages { lastAvg    :: (Int, Int)
                         , sessionAvg :: (Int, Int) } deriving Show

-- | The information needed to update the UI.
data UIEvent = NewPrompt String Averages
             | Correct   Char
             | Incorrect Char
             | DeleteRevealing Char
             | NoEvent

