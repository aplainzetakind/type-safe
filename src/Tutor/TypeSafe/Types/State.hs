{-|
Module      : Tutor.TypeSafe.Types.State
Stability   : experimental

This module contains the data types that hold the program state, and functions
to transform them.
-}
module Tutor.TypeSafe.Types.State
    -- ( DrillState (..)
    -- , initializeDrill
    -- , initializePrompt
    -- , drillDone
    -- , handleKeyPress
    -- , reportPrompt
    -- )
  where

import Tutor.TypeSafe.Types.Exception
import Tutor.TypeSafe.Types.UIEvent
import Tutor.TypeSafe.Util (diffInMs)

import System.Clock (TimeSpec)
import Data.Sequence (Seq (..), empty, (!?), (|>), fromList)
import Data.Maybe (fromMaybe)
import Control.Monad.Except
import Control.Monad.State

-- * Type synonyms

-- | 'Result' is a sequence of pressed keys, along with the time elapsed since
-- the beginning of a prompt. We have not yet implemented identifying mistakes
-- or response times. The 'Correct'/'Incorrect' events returned by various
-- functions do not distinguish between the first time a position was correctly
-- input and retyping after accidental deletions.
type Result = Seq (Char, Int)

-- | We hold the prompt text as a 'Seq', since there will likely be rather long
-- prompts, and efficient random access would be desirable. A better approach
-- might be to hold two stacks.
type Prompt = Seq Char

-- * The drill state

-- | The data type which holds the state of a running drill. This is meant to
-- change as new prompts are generated. The 'results' and 'averages' fields
-- accumulate session data.
data DrillState = DrillState
  { cursorPos :: Int        {- ^ The cursor position. -}
  , prompt    :: Prompt     {- ^ The prompt that the user needs to type. -}
  , pLength   :: Int        {- ^ The length of the prompt. -}
  , undo      :: [Char]     {- ^ A stack that holds the accumulated incorrect
                                 key presses which need to be deleted. -}
  , time      :: TimeSpec   {- ^ Time the drill started to run, used as a
                                 reference for all key presses. -}
  , result    :: Result     {- ^ Keeps the user key presses tagged
                                 by time in ms, to be processed later. -}
  , results   :: Seq (Prompt, Result)
                            {- ^ Holds the results of all the prompts of the
                                 current session -}
  , averages   :: Averages  {- ^ Holds timing data for the last prompt and the
                                 whole session. -}
  }

-- | Initial state, where everything is either empty or zero. The values here
-- are not meant to be accessed; before first use, a prompt should be
-- overwritten with 'initializePrompt'.
initializeDrill :: DrillState
initializeDrill = DrillState{ cursorPos = 0
                            , prompt    = empty
                            , pLength   = 0
                            , undo      = []
                            , time      = 0
                            , result    = empty
                            , results   = empty
                            , averages  = Averages (0,0) (0, 0)
                            }

-- | The function which takes a new prompt @Seq Char@ and a starting @TimeSpec@,
-- and overwrites relevant portions of the state (that is, everything except
-- @results@ and @averages@).
initializePrompt :: String      {- ^ The prompt to set. -}
                    -> TimeSpec {- ^ Start time. -}
                    -> DrillState -> DrillState
initializePrompt p t ds = ds { cursorPos = 0
                             , pLength   = length p
                             , prompt    = fromList p
                             , undo      = []
                             , time      = t
                             , result    = empty
                             }

-- | Takes the 'Averages' of a session and updates them with the length and
-- total time of a prompt.
updateAverages :: Int {- ^ The number of characters typed -}
                  -> Int {- ^ The time it took to type them -}
                  -> Averages -> Averages
updateAverages c t avgs@Averages{ sessionAvg = (c0, t0) }
       = avgs{ lastAvg = (c, t), sessionAvg = (c + c0, t + t0) }

-- | When a prompt is finished, 'reportPrompt' is called to 'updateAverages'
-- using the relevant fields of the 'DrillState' before a new prompt is
-- initialized.
reportPrompt :: DrillState -> DrillState
reportPrompt ds@DrillState { result = r@(_ :|> (_, t'))
                           , cursorPos = pos
                           , prompt = p
                           , undo = us
                           , results  = rs
                           , averages = avgs }
                    = ds{ results = rs |> (p, r)
                                    , averages = updateAverages c' t' avgs }
                            where c' = pos - length us
reportPrompt ds = ds -- If @result@ is empty, do nothing.

-- | Checks if the prompt is complete, which is the case if the cursor is at the
-- end with an empty error stack.
drillDone :: DrillState -> Bool
drillDone DrillState{ pLength = l, cursorPos = p, undo = [] } = l == p
drillDone _                                                   = False

-- * The @DrillS@ monad
--
-- | 'DrillS' is used to facilitate composition of transformations of
-- 'DrillState' while also supporting throwing 'TypingException's.
type DrillS a = ExceptT TypingException (State DrillState) a

-- | Returns the character at the current cursor position (which is about to be
-- written).
getCharAtPos :: DrillS Char
getCharAtPos = do DrillState{ cursorPos = p, prompt = t } <- get
                  pure $ fromMaybe ' ' $ t !? p

-- | Move the cursor right.
cursorRight :: DrillS ()
cursorRight = do ds@DrillState{ cursorPos = p } <- get
                 put ds{ cursorPos = p + 1 }

-- | Move the cursor left. Throws 'Impossible' if it's already at the beginning,
-- since it shouldn't be called in that case.
cursorLeft :: DrillS ()
cursorLeft = do ds@DrillState{ cursorPos = p } <- get
                if p == 0 then throwError $ Impossible ("cursorLeft: Already "
                                                     ++ "at the beginning.")
                          else put ds{ cursorPos = p - 1 }

-- | If 'Tutor.TypeSafe.Types.Config.blocking' is @False@, @deleteChar blocking@
-- moves the cursor to the left and returns @'DeleteRevealing' c@, where @c@ is
-- the character of the prompt at the position just deleted. If @blocking@ is
-- @True@, does nothing and returns 'NoEvent'.
deleteChar :: Bool {- ^ Blocking? -}
              -> DrillS UIEvent
deleteChar True  = pure NoEvent -- Do nothing if Blocking
deleteChar False = do DrillState{ cursorPos = p } <- get
                      if p == 0 then pure NoEvent -- Do nothing if we're at the
                                                  -- start.
                      else do cursorLeft
                              c <- getCharAtPos
                              ds <- get
                              case ds of
                                -- If there are errors to delete, delete one.
                                DrillState { undo = _ : us }
                                  -> put ds{ undo = us }
                                _ -> pure ()
                              pure $ DeleteRevealing c

-- | Checks the mistake stack and the expected character at the cursor position
-- to determine whether to return @'Correct' c@ or @'Incorrect' c@ (or 'NoEvent'
-- if 'Tutor.TypeSafe.Types.Config.blocking').
typeChar :: Char -> Bool {- ^ Blocking? -} -> DrillS UIEvent
typeChar c False = do n  <- getCharAtPos
                      cursorRight
                      ds <- get
                      case ds of
                        -- If there are mistakes to delete, accumulate more.
                        DrillState { undo = us@(_ : _) }
                          -> do put ds{ undo = c : us }
                                pure $ Incorrect c
                        _ -> if c == n
                               then pure $ Correct c
                               else put ds{ undo = [c] }
                                    >> pure (Incorrect c)
typeChar c True = do n <- getCharAtPos
                     if n == c then cursorRight >> pure (Correct c)
                               else pure NoEvent

-- | @reportKeyPress c t@ appends @(c, 'diffInMs' t 'time')@ to 'result'.
reportKeyPress :: Char -> TimeSpec -> DrillS ()
reportKeyPress c t = do ds@DrillState{ time = t0, result = res } <- get
                        let dif = diffInMs t t0
                        put ds{ result = res |> (c, dif) }

-- | This is the monadic expression of what change 'DrillState' needs to undergo
-- given an input character, the state of
-- 'Tutor.TypeSafe.Types.Config.blocking', and the time of the key press.
handleKeyPress' :: Char -> Bool {- ^ Blocking? -} -> TimeSpec -> DrillS UIEvent
handleKeyPress' k b t = do
  reportKeyPress k t
  case k of
    '\ESC' -> throwError PressedESC
    '\DEL' -> deleteChar b
    c      -> typeChar c b

-- | This is the function to be exposed for use in other modules.
-- @
-- handleKeyPress c b t = runState $ runExceptT (handleKeyPress' c b t)
-- @
handleKeyPress :: Char -> Bool -> TimeSpec -> DrillState
                  -> (Either TypingException UIEvent, DrillState)
handleKeyPress c b t = runState $ runExceptT (handleKeyPress' c b t)
