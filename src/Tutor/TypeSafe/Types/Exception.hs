
{-|
Module      : Tutor.TypeSafe.Types.Exception
Stability   : experimental

Exceptions.
-}
module Tutor.TypeSafe.Types.Exception where

-- | All exceptions.
data TypingException = PressedESC
                     | UnexpectedScreen
                     | DrillComplete
                     | Impossible String deriving Show
