{-|
Module      : Tutor.TypeSafe.Types.Typist
Stability   : experimental

The monadic computation type 'Typist'.
-}

{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Tutor.TypeSafe.Types.Typist where

import Tutor.TypeSafe.Types.State
import Tutor.TypeSafe.Types.Config
import Tutor.TypeSafe.Types.Channels
import Tutor.TypeSafe.Types.Exception

import Control.Monad.Reader (ReaderT, runReaderT)
import Control.Monad.State (StateT)
import Control.Monad.Except (ExceptT)
import Control.Monad.Trans.Class
import qualified Control.Monad.Reader as Rd
import qualified Control.Monad.State as St
import qualified Control.Monad.Except as Ex
import Control.Monad.IO.Class

-- * Typist

-- | This is the transformer stack we use for the type of
-- 'Tutor.TypeSafe.Drill.drill'.
newtype Typist a = Typist { unTypist :: ReaderT (Config, Channels)
                                          (ExceptT TypingException
                                            (StateT DrillState IO)) a }
                            deriving (Functor, Applicative, Monad, MonadIO)

-- | Unwrap the @newtype@ wrapper and 'runReaderT'.
runTypist :: Typist a -> Config -> Channels
             -> ExceptT TypingException (StateT DrillState IO) a
runTypist = curry . runReaderT . unTypist

-- * Standard functions lifted/wrapped

-- | @ask = Typist Control.Monad.Reader.ask@
ask :: Typist (Config, Channels)
ask = Typist Rd.ask

-- | @asks = Typist Control.Monad.Reader.asks@
asks :: ((Config, Channels) -> a) -> Typist a
asks = Typist . Rd.asks

-- | @get = Typist $ lift . lift $ Control.Monad.State.get@
get :: Typist DrillState
get = Typist $ lift . lift $ St.get

-- | @gets = Typist $ lift . lift $ Control.Monad.State.gets@
gets :: (DrillState -> a) -> Typist a
gets f = f <$> get

-- | @put = Typist $ lift . lift $ Control.Monad.State.put@
put :: DrillState -> Typist ()
put = Typist . lift . lift . St.put

-- | @throw = Typist . Control.Monad.Except.throwError@
throw :: TypingException -> Typist a
throw = Typist . Ex.throwError

-- | @catch (Typist m) f
-- = Typist $ m \`Control.Monad.Except.catchError\` (unTypist . f)@
catch :: Typist a -> (TypingException -> Typist a) -> Typist a
catch (Typist m) f = Typist $ m `Ex.catchError` (unTypist . f)

-- | @liftEither = Typist . Control.Monad.Except.liftEither@
liftEither :: Either TypingException a -> Typist a
liftEither = Typist . Ex.liftEither
