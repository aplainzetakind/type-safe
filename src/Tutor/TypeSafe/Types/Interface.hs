{-|
Module      : Tutor.TypeSafe.Types.Config
Stability   : experimental

General UI setup functions.
-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE FlexibleContexts          #-}
module Tutor.TypeSafe.Types.Interface where

import Tutor.TypeSafe.Types.Channels
import Tutor.TypeSafe.Types.Config
import Tutor.TypeSafe.Interfaces.CLI

import Control.Monad.IO.Class

-- * Interface
-- $ The abstactions in this module are probably overkill, and should be
-- simplified.

-- | This is a placeholder, to provide a second type/case for the following
-- types and functions.
data GUIData = GUIData { channelsGUI :: Channels }

instance HasChannels GUIData where
  getChannels = channelsGUI

-- | This contains data of multiple interfaces in a single type.
data UIData = SCLIData CLIData | SGUIData GUIData

instance HasChannels UIData where
  getChannels (SCLIData d) = getChannels d
  getChannels (SGUIData d) = getChannels d

-- | A single function to initialize any UI.
initializeUI :: UI -> IO UIData
initializeUI CLI = SCLIData <$> initializeCLI
initializeUI GUI = undefined

-- | A single function to close any UI.
closeUI :: UIData -> IO ()
closeUI (SCLIData d) = closeCLI d
closeUI (SGUIData d) = undefined d

-- | Reads the interface from a config, initializes the UI, and uses the created
-- channels along with the initial config to return a monadic action that
-- depends on these two.
withInterface :: MonadIO m => Config -> (Config -> Channels -> m a) -> m a
withInterface conf@Config{ ui = i } f = do
                       d <- liftIO $ initializeUI i
                       let cs = getChannels d
                       res <- f conf cs
                       liftIO $ closeUI d
                       pure res
