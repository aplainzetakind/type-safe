{-|
Module      : Tutor.TypeSafe.Types.Channels
Stability   : experimental

This module contains the 'Channels' type and the related type class
'HasChannels', whose purpose is to uniformize access across various types.
-}
module Tutor.TypeSafe.Types.Channels where

import Tutor.TypeSafe.Types.UIEvent
import Control.Monad.IO.Class
import Control.Concurrent.Chan (Chan, newChan)

-- * Channels
-- $ The user interface and the main program logic communicate through two
-- 'Chan's. One is used to send keys from the UI to the backend, the other is
-- used to send instructions of what UI events to render from the backend to the
-- UI.

-- | The data type holding the 'Chan's.
data Channels = Channels {
    -- | The 'Chan' used to transmit keypresses from the UI to the backend.
    charInput   :: Chan Char
    -- | The 'Chan' used to transmit 'UIEvent's to be rendered from the backend
    -- to the UI.
  , eventOutput :: Chan UIEvent }

-- | This type class is used to provide a unified interface to data structures
-- which contain 'Channels'. Intended to facilitate refactoring if those
-- structures need to be changed.
class HasChannels a where
  getChannels :: a -> Channels
  getCharChan   :: a -> Chan Char
  getCharChan = charInput . getChannels
  getEventChan  :: a -> Chan UIEvent
  getEventChan = eventOutput . getChannels
  {-# MINIMAL getChannels #-}

instance HasChannels Channels where
  getChannels = id

instance HasChannels b => HasChannels (a, b) where
  getChannels = getChannels . snd

-- | This creates 'Channels' from within any 'MonadIO'.
makeChans :: MonadIO m => m Channels
makeChans = liftIO $ Channels <$> newChan <*> newChan
