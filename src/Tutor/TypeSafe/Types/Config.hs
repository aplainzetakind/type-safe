{-|
Module      : Tutor.TypeSafe.Types.Config
Stability   : experimental

The data type for program configuration.
-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE ExistentialQuantification #-}
module Tutor.TypeSafe.Types.Config where

import Tutor.TypeSafe.PromptGen

-- | User interface options
data UI = CLI
        | GUI {- ^ Placeholder, not implemented. -}

-- | The configuration type. The field 'blocking' indicates whether to ignore
-- all mistakes and wait for the correct character. When not blocking, the
-- current implementation expects all mistakes to be corrected before a prompt
-- concludes. This is rudimentary, and an option to finish with mistakes should
-- be included.
data Config = Config { ui       :: UI
                     , blocking :: Bool
                     , promptGen :: IO String {- ^ The prompt generating 'IO'
                                                   action. -}
                     }

-- | The default configuration.
defaultConfig :: Config
defaultConfig = Config { ui        = CLI
                       , blocking  = False
                       , promptGen = randomWords 4 }

-- | A type class for unified access through data which contains 'Config' info.
class HasConfig a where
  getConfig :: a -> Config

instance HasConfig Config where
  getConfig = id

instance HasConfig a => HasConfig (a, b) where
  getConfig = getConfig . fst
