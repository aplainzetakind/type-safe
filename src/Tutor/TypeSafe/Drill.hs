{-|
Module      : Tutor.TypeSafe.Types.Drill
Stability   : experimental

This module contains 'drill', which provides the principal functionality of the
program.
-}
{-# LANGUAGE LambdaCase        #-}
module Tutor.TypeSafe.Drill where

import Tutor.TypeSafe.Types.Channels
import Tutor.TypeSafe.Types.Config
import Tutor.TypeSafe.Types.Typist
import Tutor.TypeSafe.Types.State
import Tutor.TypeSafe.Types.Exception
import Tutor.TypeSafe.Types.UIEvent

import System.Clock (getTime, Clock (..))
import Control.Monad (when)
import Control.Concurrent.Chan
import Control.Monad.IO.Class

-- | Writes a 'UIEvent' to 'eventOutput'.
sendEvent :: UIEvent -> Typist ()
sendEvent e = asks getEventChan >>= liftIO . (`writeChan` e)

-- | Does nothing. Presently used to only break out of the loop.
--
-- ==== TODO:
-- Eventually should be able to report results to the database.
exitDrill :: Typist ()
exitDrill = pure ()

-- | Generates a prompt with 'promptGen', sends the appropriate 'NewPrompt'
-- event to the UI and changes the state using 'initializePrompt'.
newPrompt :: Typist ()
newPrompt = do p <- asks (promptGen . getConfig) >>= liftIO
               t <- liftIO $ getTime Monotonic
               ds@DrillState{ averages = avgs } <- get
               sendEvent $ NewPrompt p avgs
               put $ initializePrompt p t ds

-- | Reads a 'Char' from 'charInput', calls 'handleKeyPress' to transform state.
processChar :: Typist ()
processChar = do c <- asks getCharChan >>= liftIO . readChan
                 t  <- liftIO $ getTime Monotonic
                 b  <- asks (blocking . getConfig)
                 ds <- get
                 let (ee, ds') = handleKeyPress c b t ds
                 put ds'
                 liftEither ee >>= sendEvent

-- | Gets called when 'PressedESC' or 'DrillComplete' is caught. Calls
-- 'reportPrompt'
endPrompt :: Typist ()
endPrompt = get >>= put . reportPrompt

-- | The action which keeps generating new prompts and runs a drill. Perhaps it
-- would be better to throw 'DrillComplete' from 'handleKeyPress' rather than
-- from a loop here.
drill :: Typist ()
drill = do newPrompt
           run `catch` \case
                          PressedESC    -> endPrompt >> exitDrill
                          DrillComplete -> endPrompt >> drill
                          e             -> throw e
             where run = do processChar
                            f <- drillDone <$> get
                            when f $ throw DrillComplete
                            run
