{-|
Module      : Tutor.TypeSafe.Util
Stability   : experimental

Miscellaneous utility functions.
-}
module Tutor.TypeSafe.Util where

import System.Clock (TimeSpec, sec, nsec)

-- | Converts a 'TimeSpec' into miliseconds.
timeInMs :: TimeSpec -> Int
timeInMs t = fromIntegral (1000 * sec t + nsec t) `div` 10 ^ (6 :: Int)

-- | Finds the difference of two 'TimeSpec's in miliseconds.
diffInMs :: TimeSpec -> TimeSpec -> Int
diffInMs t1 t0 = 1000 * fromIntegral (sec t1 - sec t0)
                 + fromIntegral (nsec t1 - nsec t0) `div` 10 ^ (6 :: Int)

