{-|
Module      : Tutor.TypeSafe.Interfaces.CLI
Stability   : experimental

The command line interface.
-}
module Tutor.TypeSafe.Interfaces.CLI where

import Tutor.TypeSafe.Types.Channels
import Tutor.TypeSafe.Types.UIEvent

import System.IO ( BufferMode(..)
                 , hSetBuffering
                 , hGetBuffering
                 , hSetEcho
                 , stdin
                 , stdout )
import System.Console.ANSI
import Text.Printf (printf)
import Control.Monad (forever)
import Control.Monad.IO.Class
import Control.Concurrent (ThreadId, forkIO, killThread, threadDelay)
import Control.Concurrent.Chan (readChan, writeChan)

-- | CLI runs in two threads. 'renderThread' reads the events from 'eventOutput'
-- and renders them, 'inputThread' writes key presses to 'charInput'.
data CLIThreads = CLIThreads { renderThread :: ThreadId
                             , inputThread  :: ThreadId
                             }

-- | 'CLIData' initializes and saves data that will be needed to run and close
-- the interface.
data CLIData = CLIData
                 { stdinBuf       :: BufferMode
                 , stdoutBuf      :: BufferMode {- ^ We save the 'BufferMode'
                                                     state of the terminal so
                                                     that we can restore it
                                                     when closing. -}
                 , chansCLI       :: Channels {- ^ 'Channels' to be created
                                                   when initializing the CLI. -}
                 , threads        :: CLIThreads
                 }

instance HasChannels CLIData where
  getChannels = chansCLI

-- | To initialize the CLI, we first obtain the 'BufferMode' of 'stdin and
-- 'stdout'. Then we set both to 'NoBuffering', in order to get and put
-- character-by-character. We also call @'hSetEcho' stdin False@, to override
-- key press echoing. Finally, we crate the 'Channels' and 'CLIThreads'.
initializeCLI :: IO CLIData
initializeCLI =  do inB  <- hGetBuffering stdin
                    outB <- hGetBuffering stdout
                    hSetBuffering stdin  NoBuffering
                    hSetBuffering stdout NoBuffering
                    hSetEcho stdin False
                    promptGray
                    clearScreen
                    cs   <- makeChans
                    ts   <- createCLIThreads cs
                    pure $ CLIData inB outB cs ts

-- | Used in 'closeCLI'.
killCLIThreads :: CLIThreads -> IO ()
killCLIThreads (CLIThreads r i) = killThread r >> killThread i

-- | Restores the buffering of @stdin@ and @stdout@, restores echoing of
-- @stdin@, and kills the 'CLIThreads'.
closeCLI :: CLIData -> IO ()
closeCLI d = do threadDelay 1000
                setSGR [Reset]
                putStrLn ""
                killCLIThreads $ threads d
                hSetEcho stdin True
                hSetBuffering stdin  NoBuffering
                hSetBuffering stdout NoBuffering

-- | Using 'forkIO', runs 'renderProcessCLI' and 'inputProcessCLI' in their own
-- threads. Returns the threads so they can be killed when done.
createCLIThreads :: Channels -> IO CLIThreads
createCLIThreads cs = do
        renderT <- liftIO . forkIO $ renderProcessCLI cs
        inputT  <- liftIO . forkIO $ inputProcessCLI cs
        pure $ CLIThreads renderT inputT

-- | Reads 'UIEvent's and hands them over to 'handleEvent', 'forever'
renderProcessCLI :: HasChannels a => a -> IO ()
renderProcessCLI a = forever $ readChan (getEventChan a) >>= handleEvent

-- | Reads 'Char's from @stdin@ and writes them to 'charInput', 'forever'.
inputProcessCLI :: HasChannels a => a -> IO ()
inputProcessCLI a = forever $ do c <- getChar
                                 writeChan (getCharChan a) c

-- | Sets the color for the prompt.
promptGray :: IO ()
promptGray = setSGR [ SetConsoleIntensity NormalIntensity
                    , SetPaletteColor Background $ xterm6LevelRGB 0 0 0
                    , SetPaletteColor Foreground $ xterm6LevelRGB 2 2 2 ]

-- | Sets the color for correctly typed characters.
correctGreen :: IO ()
correctGreen = setSGR [ SetConsoleIntensity BoldIntensity
                      , SetPaletteColor Background $ xterm6LevelRGB 0 0 0
                      , SetPaletteColor Foreground $ xterm6LevelRGB 2 4 2 ]

-- | Sets the color for erroneous input.
mistakeRed :: IO ()
mistakeRed = setSGR [ SetConsoleIntensity BoldIntensity
                    , SetPaletteColor Foreground $ xterm6LevelRGB 0 0 0
                    , SetPaletteColor Background $ xterm6LevelRGB 2 0 0 ]

-- | Sets the neutral color.
plain :: IO ()
plain = setSGR [ SetConsoleIntensity NormalIntensity
               , SetPaletteColor Background $ xterm6LevelRGB 0 0 0
               , SetPaletteColor Foreground $ xterm6LevelRGB 4 4 4 ]

-- | Prints the bird track which precedes the prompt.
birdTrack :: IO ()
birdTrack = setSGR [ SetConsoleIntensity BoldIntensity
                   , SetPaletteColor Background $ xterm6LevelRGB 0 0 0
                   , SetPaletteColor Foreground $ xterm6LevelRGB 5 5 5 ]
            >> setCursorPosition 3 0 >> putStr "> "

-- | Prints \"Press ESC to quit\" at the top.
pressESCNote :: IO ()
pressESCNote = cursorToTop >> plain >> putStr "Press ESC to quit"

-- | Prints the prompt after the bird track.
putPrompt :: String -> IO ()
putPrompt s = setCursorPosition 3 2 >> promptGray >> putStr s

-- | Takes the tuple @(c, t)@, where @c@ is the number of characters typed and
-- @t@ is the total time elapsed, and returns @Maybe@ characters per minute,
-- unless @t@ is zero.
getAverage :: (Int, Int) -> Maybe Int
getAverage (_, 0) = Nothing
getAverage (c, t) = Just $ (60000 * c) `div` t

-- | Prints the characters per minute for the last prompt or the whole session.
writeAverage :: String {- ^ To be used with @"Last"@ or @"Avg"@ -}
                -> (Int, Int) -> IO ()
writeAverage s a = plain
                   >> putStr (s ++ " CPM: ")
                   >> colorize ma
                   >> putStr text
          where text = maybe "N/A" (printf "%3d") ma
                ma   = getAverage a

-- |
-- @vBar = plain >> putStr " | "@
vBar :: IO ()
vBar = plain >> putStr " | "

-- | Colors a CPM value according to performance.
colorize :: Maybe Int {- ^ 'Nothing' will be colored by 'plain' -}
            -> IO ()
colorize Nothing            = plain
colorize (Just r) = setSGR [ SetConsoleIntensity BoldIntensity
                           , SetPaletteColor Background $ xterm6LevelRGB 0 0 0
                           , SetPaletteColor Foreground $ xterm6LevelRGB m n 0 ]
                  where n = (+ 1) $ min 4 $ max 0 $ (r - 50) `div` 60
                        m = 6 - n

-- | Prints the CPM for the last prompt and the session average, separated by
-- @" | "@.
putAvgs :: Averages -> IO ()
putAvgs avgs = setCursorPosition 1 0
               >> writeAverage "Last" (lastAvg avgs)
               >> vBar
               >> writeAverage "Avg" (sessionAvg avgs)

-- | Does the appropriate printing/deleting according to the 'UIEvent'.
handleEvent :: UIEvent -> IO ()
handleEvent (NewPrompt s avgs)   = do clearScreen
                                      pressESCNote
                                      putAvgs avgs
                                      birdTrack
                                      putPrompt s
                                      setCursorPosition 3 2
handleEvent (Correct   c)       = do correctGreen
                                     putChar c
handleEvent (Incorrect c)       = do mistakeRed
                                     putChar c
handleEvent (DeleteRevealing c) = do promptGray
                                     cursorBackward 1
                                     putChar c
                                     cursorBackward 1
handleEvent NoEvent = pure ()

-- | Places the cursor at the top left corner.
cursorToTop :: IO ()
cursorToTop = setCursorPosition 0 0
