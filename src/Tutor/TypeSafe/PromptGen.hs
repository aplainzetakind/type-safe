{-|
Module      : Tutor.TypeSafe.PromptGen
Stability   : experimental

Prompt generating functions.
-}
module Tutor.TypeSafe.PromptGen where

import Control.Monad (replicateM)
import System.IO ( SeekMode(..)
                 , Handle
                 , IOMode(..)
                 , hGetChar
                 , hGetLine
                 , hFileSize
                 , hTell
                 , hSeek
                 , hClose
                 , openFile )
import System.Random (randomRIO)

-- | Given a 'Handle', use 'hGetChar' to read a character. If it's @'\n'@, after
-- reading we are at the start of a new line, so 'hGetLine' to get that entire
-- line. Otherwise, get the absolute position in the file by 'hTell'. If it's,
-- @1@, we're at the first line, so skip to position @0@ and read 'hGetLine' to
-- get the first line. Otherwise, seek back 2 positions and start over.
hGetCurrentLine :: Handle -> IO String
hGetCurrentLine h = do c <- hGetChar h
                       if c == '\n'
                         then hGetLine h
                         else do p <- hTell h
                                 if p == 1
                                 then hSeek h AbsoluteSeek 0 >> hGetLine h
                                 else hSeek h RelativeSeek (-2)
                                         >> hGetCurrentLine h

-- | Jumps to a random position in the file, and calls 'hGetCurrentLine'. Very
-- heavily biased towards longer lines.
randomWord :: Handle -> IO String
randomWord h = do n <- hFileSize h
                  l <- randomRIO (0, n - 1)
                  hSeek h AbsoluteSeek l
                  hGetCurrentLine h

-- | @randomWords n@ returns a string consisting of @n@ random words from the
-- English word list @resources/words.txt@.
randomWords :: Int -> IO String
randomWords n = do h <- openFile "resources/words.txt" ReadMode
                   s <- unwords <$> replicateM n (randomWord h)
                   hClose h
                   pure s
