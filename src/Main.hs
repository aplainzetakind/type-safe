{-|
Module      : Main
Stability   : experimental
-}
module Main where

import Tutor.TypeSafe.Types.Interface
import Tutor.TypeSafe.Types.Typist
import Tutor.TypeSafe.Types.Config
import Tutor.TypeSafe.Types.State
import Tutor.TypeSafe.Drill

import Control.Monad.State
import Control.Monad.Except

-- | The entry point of the program. Runs 'drill', with the appropriate initial
-- conditions.
--
-- @
-- (\`evalStateT\` 'initializeDrill') $ runExceptT
--   $ 'withInterface' 'defaultConfig' $ runTypist 'drill'
-- @
main :: IO ()
main = do p <- (`evalStateT` initializeDrill)
               $ runExceptT
               $ withInterface defaultConfig
               $ runTypist drill
          case p of
            Left a -> print a
            Right _ -> pure ()
